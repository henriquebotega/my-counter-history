import { applyMiddleware, createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import root from "./reducers";

const composeEnhancers = composeWithDevTools({ trace: true });
export default createStore(root, composeEnhancers(applyMiddleware(thunk)));
