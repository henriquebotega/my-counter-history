import api from "../../services/fakeApi";
import { DECREASE, DECREASING, INCREASING, INCREMENT } from "../reducers/count";
import { LOG_HISTORY } from "../reducers/history";

export const increaseAction = () => async (dispatch) => {
	dispatch({ type: INCREASING });
	await api(3);
	dispatch({ type: INCREMENT });
	dispatch({
		type: LOG_HISTORY,
		payload: {
			increased: true,
		},
	});
};

export const decreaseAction = () => async (dispatch) => {
	dispatch({ type: DECREASING });
	await api(1);
	dispatch({ type: DECREASE });
	dispatch({
		type: LOG_HISTORY,
		payload: {
			increased: false,
		},
	});
};
