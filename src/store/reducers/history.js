import moment from "moment";

export const LOG_HISTORY = "LOG_HISTORY";

const addToHistory = (history, entry) => [...history].concat([entry]);

const initialState = {
	historyList: [
		{
			time: moment(),
			action: "No actions performed.",
		},
	],
};

const historyReducer = (state = initialState, action) => {
	switch (action.type) {
		case LOG_HISTORY:
			return {
				...state,
				historyList: addToHistory(state.historyList, {
					time: moment(),
					action: action.payload.increased ? "Increased by 1" : "Decreased by 1",
				}),
			};
		default:
			return state;
	}
};

export default historyReducer;
