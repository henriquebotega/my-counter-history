import { combineReducers } from "redux";
import count from "./count";
import history from "./history";

export default combineReducers({
	count,
	history,
});
