export const INCREMENT = "INCREMENT";
export const DECREASE = "DECREASE";
export const INCREASING = "INCREASING";
export const DECREASING = "DECREASING";

const initialState = {
	isIncreasing: false,
	isDecreasing: false,
	value: 0,
};

function addReducer(state = initialState, action) {
	switch (action.type) {
		case INCREASING:
			return { ...state, isIncreasing: true };
		case DECREASING:
			return { ...state, isDecreasing: true };
		case INCREMENT:
			return { ...state, isIncreasing: false, value: state.value + 1 };
		case DECREASE:
			return { ...state, isDecreasing: false, value: state.value - 1 };
		default:
			return state;
	}
}

export default addReducer;
