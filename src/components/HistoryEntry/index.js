import React from "react";
import { Header } from "semantic-ui-react";

export const HistoryEntry = ({ time, message, tinted }) => (
	<div style={EntryRootStyles(tinted)}>
		<Header as="h6" textAlign="center" style={TimeStyles}>
			{time.fromNow()}
		</Header>
		<Header as="h5" color="grey" textAlign="center" style={{ marginTop: 8 }}>
			{message}
		</Header>
	</div>
);

const EntryRootStyles = (tinted) => ({
	backgroundColor: tinted ? "#F7F7F7" : "#fff",
	width: "100%",
	padding: 8,
});

const TimeStyles = {
	color: "#c7c7c7",
	marginBottom: 0,
};
