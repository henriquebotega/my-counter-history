import React from "react";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { Header, Image } from "semantic-ui-react";
import { decreaseAction, increaseAction } from "../../store/actions";
import { HistoryEntry } from "../HistoryEntry";

const Button = ({ children, disabled, onClick }) => {
	return (
		<button type="button" className="ui button" disabled={disabled} onClick={onClick}>
			{children}
		</button>
	);
};
export const Page = () => {
	const dispatch = useDispatch();

	const {
		count: { isIncreasing, isDecreasing, value },
		history: { historyList },
	} = useSelector((state) => state, shallowEqual);

	return (
		<div style={ContainerStyles}>
			<Image src="https://cdn.iconscout.com/icon/free/png-512/redux-283024.png" style={{ width: 35, height: 35 }} />
			<Header as="h1" textAlign="center" style={CountStyles}>
				{value}
			</Header>
			<div style={ButtonContainer}>
				<Button disabled={isIncreasing} onClick={() => dispatch(increaseAction())}>
					{isIncreasing ? "updating..." : "increment"}
				</Button>
				<Button disabled={isDecreasing} onClick={() => dispatch(decreaseAction())}>
					{isDecreasing ? "updating..." : "decrement"}
				</Button>
			</div>
			<Header as="h5" color="grey" style={{ marginTop: 40 }}>
				ACTION HISTORY
			</Header>
			<div style={HistoryContainerStyles}>
				{historyList
					.sort((a, b) => (a.time.isBefore(b.time) ? 1 : -1))
					.map((entry, index) => (
						<HistoryEntry key={index} time={entry.time} message={entry.action} tinted={index % 2 === 0} />
					))}
			</div>
		</div>
	);
};

const ContainerStyles = {
	backgroundColor: "#fff",
	boxShadow: "0px 4px 18px -4px rgba(0,0,0,0.44)",
	borderRadius: 5,
	width: "80%",
	minHeight: 300,
	margin: "auto",
	marginTop: 16,
	padding: "16px 16px",
};

const HistoryContainerStyles = {
	borderTop: "1px solid #f1f1f1",
	margin: "auto",
	paddingTop: 8,
};

const ButtonContainer = {
	display: "flex",
	justifyContent: "center",
	width: "90%",
	margin: "auto",
};

const CountStyles = {
	margin: "24px 0",
};
