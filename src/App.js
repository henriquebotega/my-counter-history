import React from "react";
import { Provider } from "react-redux";
import "semantic-ui-css/semantic.min.css";
import { Page } from "./components/Page";
import store from "./store";

function App() {
	return (
		<Provider store={store}>
			<Page />
		</Provider>
	);
}

export default App;
